<!DOCTYPE html>
<html lang="ru">
@include('components.dbfshow.head')
<body>
    <div class="preloader">
        <div class="preloader__row">
          <div class="preloader__item"></div>
          <div class="preloader__item"></div>
        </div>
      </div>
    <div class="wrapper">
        <div class="columns is-fullheight">
            <main class="column main left-shadow" style='background-color: #b5b0b0;'>
                              
                <!--@include('components.dbfshow.page-tittle')-->
                                                  
                @yield('content')

            </main>
        </div>
    </div>
    <script type="text/javascript" src="/js/app.js"></script>
    <link rel="stylesheet" href="/css/app.css">    

    <script>
        window.onload = function () {
          document.body.classList.add('loaded_hiding');
          window.setTimeout(function () {
            document.body.classList.add('loaded');
            document.body.classList.remove('loaded_hiding');
          }, 500);
        }
      </script>

</body>

</html>

<!DOCTYPE html>
<html lang="ru">
@include('components.head')

<body>
    <div class="container-fluid">
        <div class="row">

            @include('components.menuleft')
            <div class="right-column">

                @include('components.header')
                <main class="main-content p-5" role="main">
                    <div class="row">
                        <div class="col-md-12">
                            <h1>@yield('title')</h1>
                        </div>
                    </div>
                    @yield('content')
                    @include('components.footer')
                </main>
            </div>

        </div>
    </div>

    <!-- SCRIPTS - REQUIRED START -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="/js/app.js"></script>
    <!-- JQuery -->
    <script type="text/javascript" src="assets/js/jquery/jquery-3.1.1.min.js"></script>
    <!-- Popper.js - Bootstrap tooltips -->
    <script type="text/javascript" src="assets/js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="assets/js/bootstrap/mdb.min.js"></script>
    <!-- Velocity -->
    <script type="text/javascript" src="assets/plugins/velocity/velocity.min.js"></script>
    <script type="text/javascript" src="assets/plugins/velocity/velocity.ui.min.js"></script>
    <!-- Custom Scrollbar -->
    <script type="text/javascript" src="assets/plugins/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- jQuery Visible -->
    <script type="text/javascript" src="assets/plugins/jquery_visible/jquery.visible.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script type="text/javascript" src="assets/js/misc/ie10-viewport-bug-workaround.js"></script>

    <!-- SCRIPTS - REQUIRED END -->

    <!-- SCRIPTS - OPTIONAL START -->

    <!-- SCRIPTS - DEMO - START -->
    <!-- Image Placeholder -->
    <script type="text/javascript" src="assets/js/misc/holder.min.js"></script>
    <!-- SCRIPTS - DEMO - END -->

    <!-- SCRIPTS - OPTIONAL END -->

    <!-- QuillPro Scripts -->
    <script type="text/javascript" src="assets/js/scripts.js"></script>
</body>

</html>

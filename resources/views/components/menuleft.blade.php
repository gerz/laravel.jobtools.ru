<nav id="sidebar" class="px-0 bg-dark bg-gradient sidebar">
    <ul class="nav nav-pills flex-column">
        <li class="logo-nav-item">
            <a class="navbar-brand" href="#">
                <img src="assets/img/logo-white.png" width="145" height="32.3" alt="QuillPro">
            </a>
        </li>
        <li>
            <h6 class="nav-header">Основное</h6>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/logs">
                <i class="batch-icon batch-icon-star"></i>
                Логи посещений
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/qrcode">
                <i class="batch-icon batch-icon-star"></i>
                QR-Codes
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/notes">
                <i class="batch-icon batch-icon-star"></i>
                Ноутбуки
            </a>
        </li>

    </ul>
</nav>

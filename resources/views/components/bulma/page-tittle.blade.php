<div class="level">
    <div class="level-left">
      <div class="level-item">
        <div class="title has-text-primary"><i class="fa @yield('fa-icon')"></i>@yield('title')</div>
      </div>
    </div>
    <div class="level-right">
      <div class="level-item">
        ...
      </div>
    </div>
  </div>

<header class="hero">
    <div class="hero-head">
        <nav class="navbar has-shadow" role="navigation" aria-label="main navigation">
            <div class="navbar-brand">

                <a class="navbar-item is--brand">
                <img class="navbar-brand-logo" src="{{asset('assets/img/logo.png')}}" alt="Bulma Admin Template logo">
                </a>
                <a class="navbar-item is-tab is-hidden-mobile is-active"><span class="icon is-medium"><i class="fa fa-home"></i></span>Домой</a>
                <a class="navbar-item is-tab is-hidden-mobile" href="https://bitbucket.org/gerz/laravel.jobtools.ru/src/master/">Git</a>
                <a class="navbar-item is-tab is-hidden-mobile" href="http://jobtools.ru/ob-avtore/">Автора</a>

                <button class="button navbar-burger" data-target="navMenu">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>

            </div>
            <div class="navbar-menu navbar-end" id="navMenu">
                <a class="navbar-item is-tab is-hidden-tablet is-active">Домой</a>
                <a class="navbar-item is-tab is-hidden-tablet" href="https://bitbucket.org/gerz/laravel.jobtools.ru/src/master/">Git</a>
                <a class="navbar-item is-tab is-hidden-tablet" href="http://jobtools.ru/ob-avtore/">Автора</a>
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link" href="https://mazipan.space/">
                        <figure class="image is-32x32" style="margin-right:.5em;">
                            <img src="">
                        </figure>
                        Гость
                    </a>

                    <div class="navbar-dropdown is-right">
<!---                        <a class="navbar-item">
                            <span class="icon is-small">
                                <i class="fa fa-user-o"></i>
                            </span>
                            &nbsp; Profile
                        </a>
                        <hr class="navbar-divider"> -->
                        <a class="navbar-item">
                            <span class="icon is-small">
                                <i class="fa fa-power-off"></i>
                            </span>
                            &nbsp; Вход
                        </a>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>

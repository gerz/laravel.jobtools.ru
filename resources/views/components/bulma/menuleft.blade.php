<aside class="aside column is-2 right-shadow">
    <nav class="menu">
      <p class="menu-label">
        Основное
      </p>
      <ul class="menu-list">
        <li><a class="{{ request()->is('logs') ? 'is-active' : null }}" href="/logs"><span class="icon is-small"><i class="fa fa-archive"></i></span> Логи посещений</a></li>
        <li><a class="{{ request()->is('qrcode') ? 'is-active' : null }}" href="/qrcode"><span class="icon is-small"><i class="fa fa-qrcode"></i></span> QR-Codes</a></li>
        <li><a class="{{ request()->is('notes') ? 'is-active' : null }}" href="/notes"><span class="icon is-small"><i class="fa fa-laptop"></i></span> Ноутбуки</a></li>
      </ul>
    </nav>
  </aside>

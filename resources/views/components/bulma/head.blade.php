<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    {{csrf_field()}}
    <link rel="icon" href="assets/img/favicon.png">

    <title>Изучаем Laravel</title>
  
    <link rel="stylesheet" href="css/preloader.css">
    <!-- <link rel="stylesheet" href="https://cdn.materialdesignicons.com/5.3.45/css/materialdesignicons.min.css"> -->
        
    <link rel="stylesheet" href="assets/fonts/font-awesome/css/font-awesome.min.css">
</head>

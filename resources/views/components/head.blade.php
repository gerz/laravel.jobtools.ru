<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/img/favicon.png">

    <title>Изучаем Laravel</title>

    <!-- Fonts -->
    <!--<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700&amp;subset=latin-ext"
          rel="stylesheet">-->

    <!-- CSS - REQUIRED - START -->
    <!-- Batch Icons -->
    <link rel="stylesheet" href="assets/fonts/batch-icons/css/batch-icons.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap/bootstrap.min.css">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="assets/css/bootstrap/mdb.min.css">
    <!-- Custom Scrollbar -->
    <link rel="stylesheet" href="assets/plugins/custom-scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- Hamburger Menu -->
    <link rel="stylesheet" href="assets/css/hamburgers/hamburgers.css">

    <link rel="stylesheet" href="assets/css/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link href="{{ asset('assets/css/jqwidgets/styles/jqx.material.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/jqwidgets/styles/jqx.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <!-- CSS - REQUIRED - END -->


    <!-- CSS - OPTIONAL - START -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/fonts/font-awesome/css/font-awesome.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/fonts/font-awesome/css/font-awesome.min.css">
    <!-- JVMaps -->
    <link rel="stylesheet" href="assets/plugins/jvmaps/jqvmap.min.css">
    <!-- CSS - OPTIONAL - END -->

    <!-- QuillPro Styles -->
    <link rel="stylesheet" href="assets/css/quillpro/quillpro.css">
</head>

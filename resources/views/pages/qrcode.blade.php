@extends ('layouts.app-bulma')

@section('title','QR-Codes')
@section('content')

<div class="columns is-multiline">
    <div class="column">

        <h3>QR-Code с текстом 'Добро пожаловать на jobtools.ru'</h3>
        {!! QrCode::encoding('UTF-8')->size(200)->generate('Добро пожаловать на jobtools.ru'); !!}<br>
        <br>
        <h3>Цветной</h3><br>
        {!! QrCode::encoding('UTF-8')->size(200)->backgroundColor(245, 245, 220)->color(255, 0, 0)->generate('Добро пожаловать на jobtools.ru'); !!}<br>
        <br>
        <h3>С картинкой внутри</h3>
        <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->merge('image.png', 0.3, true)
                ->size(500)->errorCorrection('H')
                ->generate('http://jobtools.ru')) !!} "><br>

        <br>
        {!! QrCode::email(null, 'HellO', 'Текст письма'); !!}


    </div>
</div>

@endsection

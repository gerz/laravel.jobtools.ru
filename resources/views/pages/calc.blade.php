@extends ('layouts.app')

@section('content')

<div class="row mb-4">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Калькулятор</div>
            <div class="card-body">
                <div id="app">
                    <app></app>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

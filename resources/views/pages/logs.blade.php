@extends ('layouts.app-bulma')

@section('title','Лог посещений сайта')

@section('content')

<div class="columns is-multiline">
    <div class="column">
        <table class="table">
            <thead>
                <tr>
                    <th>time</th>
                    <th>duration</th>
                    <th>ip</th>
                    <th>url</th>
                    <th>method</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($logs as $log)
                <tr>
                    <td>{{ $log->time }}</td>
                    <td>{{ $log->duration }}</td>
                    <td>{{ $log->ip }}</td>
                    <td>{{ $log->url }}</td>
                    <td>{{ $log->method }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $logs->links() }}
    </div>
</div>

@endsection

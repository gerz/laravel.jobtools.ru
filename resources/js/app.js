require('./bootstrap');

window.Vue = require('vue');
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

Vue.use(Buefy);

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

//import App from './Components/App'
import Note from './Components/Note2'
import Dbfshow from './Components/Dbfshow'
import Menudbfshow from './Components/MenuDbfShow'
import AboutDbfShow from './Components/AboutDbfShow'
import UploadDbfShow from './Components/UploadDbfShow'

const app = new Vue({
    el: '#app',
    components: {Note, Dbfshow, Menudbfshow, UploadDbfShow, AboutDbfShow},
    theme:'bootstrap'
});

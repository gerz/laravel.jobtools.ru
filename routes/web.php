<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'LogController@show');
Route::get('/api/get-notes', 'NoteController@get');
Route::get('/logs', 'LogController@show');
Route::get('/notes', 'NoteController@show');
Route::get('/calc', 'CalcController@show');
Route::get('/dbfshow', 'DbfshowController@dbfshow');
Route::post('/dbfshow/api/show', 'DbfshowController@show');
Route::post('/dbfshow/api/upload', 'DbfshowController@upload');
Route::get('/qrcode', 'QRCodeController@show');
Route::get('qr-code', function () {
    $pngImage = QrCode::format('png')->merge('image.png', 0.3, true)
                        ->size(500)->errorCorrection('H')
                        ->generate('http://jobtools.ru');

        return response($pngImage)->header('Content-type','image/png');
});

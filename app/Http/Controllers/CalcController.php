<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log;

class CalcController extends Controller
{
    public function show()
    {
        return view('pages.calc');
    }
}

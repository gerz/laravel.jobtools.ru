<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Note;
use Illuminate\Http\Response;

class NoteController extends Controller
{
    public function show()
    {
        $note = Note::select(['name','ram_size', 'ram_type', 'cpu_type'])->paginate(10);
        return view('pages.note')->with(compact('note'));
    }
    
    public function get(Request $request)
    {
        if ($request->isMethod('get'))
        {  
            $data = $request->input();
            
             $this->validate($request, [ 'page'=>'numeric', 'per_page'=>'numeric']);

            if (!isset($data['per_page']))
                $per_page=10;
                else
                    $per_page=$data['per_page'];
            //$note = Note::select(['id_note','name','ram_size', 'ram_type', 'cpu_type'])->paginate($per_page);
            $note = Note::select()->paginate($per_page);
            return $note;

        }
    }

}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use XBase\TableReader;
use App\Services\Dbfshow;
use Storage;

class DbfshowController extends Controller
{   
    public function dbfshow(Request $request)
    {
        return view('pages.dbfshow');
    }

    public function show(Request $request)
    {
        $request->validate([
            'recordstartindex' => 'integer',
            'pagesize' => 'integer'
        ]);
        $data = $request->input();
        $recordStartIndex = $data['recordstartindex'];
        $pageSize=$data['pagesize'];
        $request->filename;
        $table = new TableReader(Storage::disk('public')->path($request->filename));
        $data =[];
        $array_records =[];
        $columns = $table->getColumns();
        if ($table->getRecordCount()>0)
        {
            $codepage = 'Windows-1251';
            if ($table->getCodePage()==254)
                $codepage = 'UTF-8';
            if (($table->getCodePage()==101) || ($table->getCodePage()==38))
                $codepage = 'CP866';

            $table->moveTo($recordStartIndex);
            $record= $table->pickRecord($recordStartIndex);
            for ($x=0; $x<$pageSize; $x++)
            {            
                foreach($columns as $col){
                    if ($record!=null)
                    {
                        $column_name = mb_convert_encoding($col->GetName(), 'UTF-8', 'Windows-1251');;
                       
                        if ($record->get($col->GetName())==null)
                            $data[$column_name] = null;                            
                        else
                        switch($col->getType())
                        {
                            case "D": //$data[$column_name] = \DateTime::createFromFormat('yymd', $record->get($col->GetName()))->format('d.m.Y');                                        
                                if (trim($record->get($col->GetName()))=="")
                                    $data[$column_name]='';
                                    else
                                    $data[$column_name] = $record->getDateTimeObject($col->GetName())->format('d.m.Y');
                                break;
                            case "T": 
                                    $data[$column_name] = $record->getDateTimeObject($col->GetName())->format('d.m.Y H:i:s');
                                break;
                            case "0": $data[$column_name]=""; 
                                break;
                            case "L": $data[$column_name] = mb_convert_encoding($record->get($col->GetName()),'UTF-8',$codepage);
                                break;
                         default: $data[$column_name] = mb_convert_encoding($record->get($col->GetName()),'UTF-8',$codepage);
                                break;
                        }                                
                    }
                    if ($record->isDeleted())
                        $data["_is_deleted81"]=1;
                    else
                        $data["_is_deleted81"]=0;
                }
                //$record = $table->nextRecord();
                //$record = $table->pickRecord($table->getRecordPos()+1);
                $record = $table->moveTo($table->getRecordPos()+1);
                array_push($array_records, $data);
                $data=[];
            }            
        }
        echo json_encode(['data'=>$array_records]);
    }


    public function upload(Request $request)
    {
        $request->validate([
            'dbf' => 'required|file|'
        ]);
        $contents = file_get_contents($request->dbf->path());
        $newPath = $request->dbf->store('TMPFOLDER', 'public');
        $columns =[];
        $datafields =[];
        $table = new TableReader(Storage::disk('public')->path($newPath));
        $cols=$table->getColumns();
        $cnt=0;
        
        $types=["C"=>"string", "L"=>"bool", "N"=>"number", "F"=>"number","I"=>"number","O"=>"number","+"=>"number", "D"=>"date","T"=>"date","@"=>"date", "M"=>"string","0"=>"string"];
                
        foreach ($cols as $col)
        {
            if ($col->GetType()!="0")
            {
                $column_name= mb_convert_encoding($col->GetName(), 'UTF-8', 'Windows-1251');
                array_push($datafields, ['name' => $column_name, 'type'=> $types[$col->GetType()]]);

                if ($col->GetType() == 'D')        
                    array_push($columns, ['text' => $column_name, 'datafield'=>$column_name, 'width'=>$col->GetLength()*10, 'cellsformat'=>'dd.MM.yyyy']);
                else
                    //array_push($columns, ['text' => $col->GetName(), 'datafield'=> $col->GetName(), /*'width'=>$col->GetLength()*10*/]);
                    array_push($columns, ['text' => $column_name, 'datafield'=> $column_name, 'width'=>$col->GetLength()*7, 'cellsformat'=>'dd.MM.yyyy hh:mm:ss']);
                $cnt++;

            }
        }

        array_push($datafields,['name'=>'_is_deleted81', 'type'=>'number']);
        //array_push($columns,['text'=>'_is_deleted81', 'datafield'=>'_is_deleted81']);

        $dbfshow = new Dbfshow();        

        $info=[];
        $info['columns_count'] = $table->getColumnCount();
        $info['rows_count'] = $table->getRecordCount();
        $info['filename'] = $newPath;
        $info['version'] = $dbfshow->getVersionFile($table->getVersion());
        $info['encoding'] = $dbfshow->getEncoding($table->getCodePage());
        
        $data = [
            'info'=>$info,
            'columns' => $columns,
            'datafields' => $datafields,
        ];
        return $data;
    }
}

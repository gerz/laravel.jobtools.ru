<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use XBase\TableReader;
use Storage;

class DbfshowController extends Controller
{   
    public function dbfshow(Request $request)
    {
        return view('pages.dbfshow');
    }

    public function show(Request $request)
    {
        /*$request->validate([
            'dbf' => 'required|file|'
        ]);*/        
        $request->filename;
        //$table = new Table(Storage::disk('public')->path($request->filename));
        $table = new TableReader(Storage::disk('public')->path($request->filename));
        $data =[];
        $array_records =[];
        $columns = $table->getColumns();
        if ($table->getRecordCount()>0)
        {
            $codepage = 'Windows-1251';
            if ($table->getCodePage()==101)
                $codepage = 'CP866';

            $record= $table->pickRecord(0);
            //for ($x=0; $x<20; $x++)
            for ($x=0; $x<100; $x++)
            {            
                foreach($columns as $col)            
                    if ($record!=null)
                        array_push($data, mb_convert_encoding($record->get($col->GetName()),'UTF-8',$codepage));
                        //$data.= "'".mb_convert_encoding($record->get($col->GetName()),'UTF-8',$codepage)."',";
                    //$data+= [$col->GetName() => mb_convert_encoding($record->get($col->GetName()),'UTF-8',$codepage)];
                $record = $table->nextRecord();
                //array_push($data, [$col->GetName()=> mb_convert_encoding($rec->get($col->GetName()),'UTF-8', 'UTF-8')]);
                array_push($array_records, $data);
                $data=[];
            }
        }
        echo json_encode(['data'=>$array_records]);
    }


    public function upload(Request $request)
    {
        $request->validate([
            'dbf' => 'required|file|'
        ]);
        $contents = file_get_contents($request->dbf->path());
        $newPath = $request->dbf->store('TMPFOLDER', 'public');
        $columns =[];
        $table = new TableReader(Storage::disk('public')->path($newPath));
        $cols=$table->getColumns();
        $cnt=0;
        
        $types=["C"=>"text", "L"=>"checkbox", "N"=>"numeric", "D"=>"date", "M"=>"text"];
        $columns_name=[];
        foreach ($cols as $col)
        {
            array_push($columns, ['data' => $cnt, 'type'=> $types[$col->GetType()]]);
            array_push($columns_name, $col->Getname());
            $cnt++;
        }
        //$col->GetName()
        $data = [
            'total' =>$table->getRecordCount(),
            'filename'=> $newPath,
            'columns' => $columns,
            'columns_name' => $columns_name
        ];
        return $data;
    }
}

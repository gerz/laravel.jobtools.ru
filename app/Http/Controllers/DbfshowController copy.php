<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use XBase\Table;
use Storage;

class DbfshowController extends Controller
{
    public function dbfshow(Request $request)
    {
        return view('pages.dbfshow');
    }

    public function show(Request $request)
    {
        /*$request->validate([
            'dbf' => 'required|file|'
        ]);*/        
        $request->filename;
        $table = new Table(Storage::disk('public')->path($request->filename));
        $data =[];
        $array_records =[];
        $columns = $table->getColumns();
        if ($table->getRecordCount()>0)
        {
            $codepage = 'Windows-1251';
            if ($table->getCodePage()==101)
                $codepage = 'CP866';

            $record= $table->pickRecord(0);
            for ($x=0; $x<20; $x++)
            {            
                foreach($columns as $col)            
                    if ($record!=null)
                        $data+= [$col->GetName() => mb_convert_encoding($record->get($col->GetName()),'UTF-8',$codepage)];
                        
                $record = $table->nextRecord();
                //array_push($data, [$col->GetName()=> mb_convert_encoding($rec->get($col->GetName()),'UTF-8', 'UTF-8')]);
                array_push($array_records, $data);
                $data=[];
            }
        }
        echo json_encode(['data'=>$array_records]);
    }


    public function upload(Request $request)
    {
        $request->validate([
            'dbf' => 'required|file|'
        ]);
        $contents = file_get_contents($request->dbf->path());
        $newPath = $request->dbf->store('TMPFOLDER', 'public');
        $columns =[];
        $table = new Table(Storage::disk('public')->path($newPath));
        $cols=$table->getColumns();
        foreach ($cols as $col)
        {
            array_push($columns, ['field' => $col->GetName(),'label' => $col->GetName()]);
        }
        $data = [
        'total' =>$table->getRecordCount(),
        'filename'=> $newPath,
        'columns' => $columns];
        return $data;
    }
}

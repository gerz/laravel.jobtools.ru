<?php
namespace App\Services;

class Dbfshow
{
    public function GetVersionFile($version)
    {
        switch ($version)
        {
            case 2: return "FoxBASE";break;
            case 3: return "FoxPro, FoxBASE+, dBASE III PLUS, dBASE IV (без memo)";break;
            case 48: return "Visual FoxPro";break;
            case 49: return "Visual FoxPro";break;
            case 67: return "dBASE IV SQL файлы  (без memo)";break;
            case 99: return "dBASE IV SQL system file  (без memo)";break;
            case 131: return "FoxBASE+, dBASE III PLUS  (с memo)";break;
            case 139: return "dBASE IV  (с memo)";break;
            case 203: return "dBASE IV ASQL table file  (с memo)";break;
            case 245: return "FoxPro 2.x  (или более ранних версий)  (с memo)";break;
            case 251: return "FoxBASE";break;
        }
        return "Unknown";    
    }

    function JulianDate($buffer)
    {
        $format = 'l1DAYS';
        $julian = unpack($format, $buffer);
        $julian = $julian["DAYS"];
        $julian = $julian - 1721119; 
        $calc1 = 4 * $julian - 1; 
        $year = floor($calc1 / 146097); 
        $julian = floor($calc1 - 146097 * $year); 
        $day = floor($julian / 4); 
        $calc2 = 4 * $day + 3; 
        $julian = floor($calc2 / 1461); 
        $day = $calc2 - 1461 * $julian; 
        $day = floor(($day + 4) / 4); 
        $calc3 = 5 * $day - 3; 
        $month = floor($calc3 / 153); 
        $day = $calc3 - 153 * $month; 
        $day = floor(($day + 5) / 5); 
        $year = 100 * $year + $julian; 

        if ($month < 10) { 
           $month = $month + 3; 
        }        
        else { 
            $month = $month - 9; 
            $year = $year + 1; 
        } 

        if (strlen($month)<=1)
            $month = '0'.$month;
        if (strlen($day)<=1)
            $day = '0'.$day;
        
        return "$day.$month.$year"; 
    }

    public function GetEncoding($enc)
    {
    /*    "0", "0", "None"));
        "1", "437", "US MS-DOS"));
        "2", "850", "International MS-DOS"));
        "3", "1252", "Windows ANSI Latin I"));
        "4", "10000", "Standard Macintosh"));
        "8", "865", "Danish OEM"));
        "9", "437", "Dutch OEM"));
        "10", "850", "Dutch OEM*"));
        "11", "437", "Finnish OEM"));
        "13", "437", "French OEM"));
        "14", "850", "French OEM*"));
        "15", "437", "German OEM"));
        "16", "850", "German OEM*"));
        "17", "437", "Italian OEM"));
        "18", "850", "Italian OEM*"));
        "19", "932", "Japanese Shift-JIS"));
        "20", "850", "Spanish OEM*"));
        "21", "437", "Swedish OEM"));
        "22", "850", "Swedish OEM*"));
        "23", "865", "Norwegian OEM"));
        "24", "437", "Spanish OEM"));
        "25", "437", "English OEM (Great Britain)"));
        "26", "850", "English OEM (Great Britain)*"));
        "27", "437", "English OEM (US)"));
        "28", "863", "French OEM (Canada)"));
        "29", "850", "French OEM*"));
        "31", "852", "Czech OEM"));
        "34", "852", "Hungarian OEM"));
        "35", "852", "Polish OEM"));
        "36", "860", "Portuguese OEM"));
        "37", "850", "Portuguese OEM*"));
        "38", "866", "Russian OEM"));
        "55", "850", "English OEM (US)*"));
        "64", "852", "Romanian OEM"));
        "77", "936", "Chinese GBK (PRC)"));
        "78", "949", "Korean (ANSI/OEM)"));
        "79", "950", "Chinese Big5 (Taiwan)"));
        "80", "874", "Thai (ANSI/OEM)"));
        s("87", "877", "ANSI"));
        "88", "1252", "Western European ANSI"));
        "89", "1252", "Spanish ANSI"));
        "100", "852", "Eastern European MS-DOS"));
        "101", "866", "Russian MS-DOS"));
        "102", "865", "Nordic MS-DOS"));
        "103", "861", "Icelandic MS-DOS"));
        "104", "895", "Kamenicky (Czech) MS-DOS"));
        "105", "620", "Mazovia (Polish) MS-DOS"));
        "106", "737", "Greek MS-DOS (437G)"));
        "107", "857", "Turkish MS-DOS"));
        "108", "863", "French-Canadian MS-DOS"));
        "120", "950", "Taiwan Big 5"));
        "121", "949", "Hangul (Wansung)"));
        "122", "936", "PRC GBK"));
        "123", "932", "Japanese Shift-JIS"));
        "124", "874", "Thai Windows/MS–DOS"));
        "134", "737", "Greek OEM"));
        "135", "852", "Slovenian OEM"));
        "136", "857", "Turkish OEM"));
        "150", "10007", "Russian Macintosh"));
        "151", "10029", "Eastern European Macintosh"));
        "152", "10006", "Greek Macintosh"));
        "200", "1250", "Eastern European Windows"));
        "201", "1251", "Russian Windows"));
        "202", "1254", "Turkish Windows"));
        "203", "1253", "Greek Windows"));
        "204", "1257", "Baltic Windows"));
        "254", "65001", "UTF-8"));*/
        switch($enc)
        {
            case 0: return "0 - None";break;
            case 1: return "437 -US MS-DOS";break;
            case 2: return "850 - International MS-DOS";break;
            case 3: return "1252 - Windows ANSI Latin I";break;
            case 4: return "10000 - Standard Macintosh";break;
            case 8: return "865 - Danish OEM";break;
            case 9: return "437 - Dutch OEM";break;
            case 10: return "850 - Dutch OEM*";break;
            case 11: return "437 - Finnish OEM";break;
            case 13: return "437 - French OEM";break;
            case 14: return "850 - French OEM*";break;
            case 15: return "437 - German OEM";break;
            case 16: return "850 - German OEM*";break;
            case 17: return "437 - Italian OEM";break;
            case 18: return "850 - Italian OEM*";break;
            case 19: return "932 - Japanese Shift-JIS";break;
            case 20: return "850 - Spanish OEM*";break;
            case 21: return "437 - Swedish OEM";break;
            case 22: return "850 - Swedish OEM*";break;
            case 23: return "865 - Norwegian OEM";break;
            case 24: return "437 - Spanish OEM";break;
            case 25: return "437 - English OEM (Great Britain)";break;
            case 26: return "850 - English OEM (Great Britain)*";break;
            case 27: return "437 - English OEM (US)";break;
            case 28: return "863 - French OEM (Canada)";break;
            case 29: return "850 - French OEM*";break;
            case 31: return "852 - Czech OEM";break;
            case 34: return "852 - Hungarian OEM";break;
            case 35: return "852 - Polish OEM";break;
            case 36: return "860 - Portuguese OEM";break;
            case 37: return "850 - Portuguese OEM*";break;
            case 38: return "866 - Russian OEM";break;
            case 55: return "850 - English OEM (US)*";break;
            case 64: return "852 - Romanian OEM";break;
            case 77: return "936 - Chinese GBK (PRC)";break;
            case 78: return "949 - Korean (ANSI/OEM)";break;
            case 79: return "950 - Chinese Big5 (Taiwan)";break;
            case 80: return "874 - Thai (ANSI/OEM)";break;
            case 87: return "877 - ANSI";break;
            case 88: return "1252 - Western European ANSI";break;
            case 89: return "1252 - Spanish ANSI";break;
            case 100: return "852 - Eastern European MS-DOS";break;
            case 101: return "866 - Russian MS-DOS";break;
            case 102: return "865 - Nordic MS-DOS";break;
            case 103: return "861 - Icelandic MS-DOS";break;
            case 104: return "895 - Kamenicky (Czech) MS-DOS";break;
            case 105: return "620 - Mazovia (Polish) MS-DOS";break;
            case 106: return "737 - Greek MS-DOS (437G)";break;
            case 107: return "857 - Turkish MS-DOS";break;
            case 108: return "863 - French-Canadian MS-DOS";break;
            case 120: return "950 - Taiwan Big 5";break;
            case 121: return "949 - Hangul (Wansung)";break;
            case 122: return "936 - PRC GBK";break;
            case 123: return "932 - Japanese Shift-JIS";break;
            case 124: return "874 - Thai Windows/MS–DOS";break;
            case 134: return "737 - Greek OEM";break;
            case 135: return "852 - Slovenian OEM";break;
            case 136: return "857 - Turkish OEM";break;
            case 150: return "10007 - Russian Macintosh";break;
            case 151: return "10029 - Eastern European Macintosh";break;
            case 152: return "10006 - Greek Macintosh";break;
            case 200: return "1250 - Eastern European Windows";break;
            case 201: return "1251 - Russian Windows";break;
            case 202: return "1254 - Turkish Windows";break;
            case 203: return "1253 - Greek Windows";break;
            case 204: return "1257 - Baltic Windows";break;
            case 254: return "65001 - UTF-8";break;
        }
        return "unknown";
    }
}